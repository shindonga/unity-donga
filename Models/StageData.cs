using System;
namespace DotnetCoreServer.Models
{
    public class StageData
    {
        public Int64 UserID { get; set; }
        public int Point { get; set; }
    }
}